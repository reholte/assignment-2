# trivia-game

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Inspiration: 
https://github.com/christiankozalla/vue-quiz-tutorial/tree/tutorial 

### Heroku 
https://hidden-falls-35956.herokuapp.com/

