import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state() {
        return {
            correctAnsweredQuestions: 0, 
            correctAnswers: [], 
            usersAnswers: [], 
            questions: [], 
        }
    }, 
    mutations: {
        updateCorrectAnsweredQuestions(state, value){
            state.correctAnsweredQuestions += value; 
        },
        updateCorrectAnswers(state, answer){
            state.correctAnswers.push(answer);
        },
        updateUsersAnswers(state, answer){
            state.usersAnswers.push(answer);
        },
        updateQuestions(state, question){
            state.questions.push(question);
        }, 
        deleteAll(state){
            state.correctAnsweredQuestions = 0;
            state.correctAnswers = [];
            state.usersAnswers = [];
            state.questions = []; 
        }
    }
}
)