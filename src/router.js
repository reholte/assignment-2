import Vue from 'vue'
import VueRouter from 'vue-router'
import QuestionConfigure from './components/Question/QuestionConfigure.vue'
import Questions from './components/Question/Questions.vue'
import Results from './components/Results.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/questions',
        name: 'Questions', 
        component: Questions 
    },
    {
        path: '/questionCongigure',
        name: 'Question Configure',
        alias: '/',
        component: QuestionConfigure
    },
    {
        path: '/results',
        name: 'Results', 
        component: Results 
    },
]; 

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL, 
    routes
})

export default router; 
